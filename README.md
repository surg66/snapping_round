# Desctiption
Snapping round - Don't Starve Together modification.

The mod creates anchor points around the circle, with the help of them you can put buildings, plant cones, etc. exactly around the circle, the reversal of farms is supported.
To set the anchor center, hover over any object and press the F10 key (by default), you can assign a key in the mod settings section.
To remove the binding, open the console and enter the command: cl_unsetsr()
To change the binding parameters, open the console and enter the command: cl_setsr(8, 32)
where 8 is the radius and 32 is the number of segments.

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1960500477)