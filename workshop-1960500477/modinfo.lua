name                       = "Snapping round"
description                = "Snapping round, version 1.0"
author                     = "surg"
version                    = "1.0"
forumthread                = ""
api_version_dst            = 10
icon_atlas                 = "modicon.xml"
icon                       = "modicon.tex"
priority                   = 0
dont_starve_compatible     = false
reign_of_giants_compatible = false
shipwrecked_compatible     = false
dst_compatible             = true
all_clients_require_mod    = false
client_only_mod            = true
server_filter_tags         = {""}

local keylist = {}
local keys = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12"}

for i = 1, #keys do
    keylist[i] = {description = keys[i], data = "KEY_"..keys[i]}
end

configuration_options =
{
    {
        name = "snap_key",
        label = "Snap key",
        hover = "Press key for snap round",
        options = keylist,
        default = "KEY_F10",
    },
}
