local _G      = GLOBAL
local require = _G.require

Assets =
{
   Asset("ANIM", "anim/buildgridplacer.zip"),
}

PrefabFiles =
{
    "snappinground",
}

local _centerinst = nil
local _coords = nil
local _radius = 4
local _segments = 16
local _points = {}
local _snap = nil
local SNAP_KEY = GetModConfigData("snap_key")

local function GetSnap(point)
    if _snap ~= nil then
        point.x = _snap.x
        point.z = _snap.z
    end

    return point
end

local function GetRotate(rot)
    if _snap ~= nil then
        rot = _snap.rot
    end
    return rot
end

local function AddCenter(x, z)
    _centerinst = _G.SpawnPrefab("snappinground")
    _centerinst.Transform:SetPosition(x, 0, z)
end

local function AddPoint(x, z, rot)
    local inst = _G.SpawnPrefab("snappinground")
    inst.Transform:SetPosition(x, 0, z)
    table.insert(_points, {inst = inst, x = x, z = z, rot = rot})
end

local function RemovePoints()
    for i, v in ipairs(_points) do
        if v.inst ~= nil then
            v.inst:Remove()
        end
    end

    if _centerinst ~= nil then
        _centerinst:Remove()
    end

    _centerinst = nil
    _points = {}
end

local function BuildRound()
    AddCenter(_coords.x, _coords.z)

    for i = 1, _segments do
        local angle = 2 * _G.PI * i / _segments
        local x, z = math.cos(angle) * _radius, math.sin(angle) * _radius
        local rot = angle / _G.DEGREES - 90
        if rot > 180 then
            rot = -360 + rot
        end
        rot = -rot
                
        AddPoint(_coords.x + x, _coords.z + z, rot)
    end
end

AddComponentPostInit("placer", function(self)
    local original_OnUpdate = self.OnUpdate

    self.OnUpdate = function(self, dt)
        local res = original_OnUpdate(self, dt)
        local foundwall = string.match(self.inst.prefab, "^wall_(.+)_item_placer$")
        if _coords ~= nil and (self.inst.prefab ~= "fence_item_placer" and self.inst.prefab ~= "fence_gate_item_placer" and not foundwall) then
            _snap = nil
            local x, y, z = self.inst.Transform:GetWorldPosition()
            local distance = math.sqrt(((_coords.x - x) ^ 2) + ((_coords.z - z) ^ 2))

            if distance - 0.5 <= _radius and distance + 0.5 >= _radius then
                local mindist = _radius * 2.5
                for i, v in ipairs(_points) do
                    if v.inst ~= nil then
                        local dist = math.sqrt(((v.x - x) ^ 2) + ((v.z - z) ^ 2))
                        if mindist > dist then
                            local sx, _, sz = v.inst.Transform:GetWorldPosition()
                            _snap = {x = sx, z = sz, rot = v.rot}
                            mindist = dist
                        end
                    end
                end

                if _snap ~= nil then
                    self.inst.Transform:SetPosition(_snap.x, 0, _snap.z)
                    self.inst.Transform:SetRotation(_snap.rot)
                end
            end
        end

        return res
    end
end)

AddComponentPostInit("deployable", function(self)
    local original_CanDeploy = self.CanDeploy
    local original_Deploy = self.Deploy

    self.CanDeploy = function(self, pt, mouseover, ...)
        pt = GetSnap(pt)
        return original_CanDeploy(self, pt, nil, ...)
    end
    
    self.Deploy = function(self, pt, deployer, ...)
        pt = GetSnap(pt)
        return original_Deploy(self, pt, deployer, ...)
    end
end)

AddClassPostConstruct("components/builder_replica", function(self)
    local original_CanBuildAtPoint = self.CanBuildAtPoint
    local original_MakeRecipeAtPoint = self.MakeRecipeAtPoint
    
    self.CanBuildAtPoint = function(self, pt, recipe, rot)
        pt = GetSnap(pt)
        rot = GetRotate(rot)
        return original_CanBuildAtPoint(self, pt, recipe, rot)
    end

    self.MakeRecipeAtPoint = function(self, recipe, pt, rot, skin)
        pt = GetSnap(pt)
        rot = GetRotate(rot)
        original_MakeRecipeAtPoint(self, recipe, pt, rot, skin)
    end
end)

AddClassPostConstruct("components/playeractionpicker", function(self)
	local original_DoGetMouseActions = DoGetMouseActions

	self.DoGetMouseActions = function(self, position, target)
		local lmb, rmb = original_DoGetMouseActions(self, position, target)

		if rmb.action == ACTIONS.DEPLOY then
			rmb.pos = GetSnap(rmb.pos)
			rmb.rotation = GetRotate(rmb.rotation)
		end

		return lmb, rmb
	end
end)
--[[
local original_SendRPCToServer = _G.SendRPCToServer
_G.SendRPCToServer = function(rpc, action_code, x, z, target, rotation, ...)
    if rpc == _G.RPC.RightClick and action_code == _G.ACTIONS.DEPLOY.code then
        local ThePlayer = _G.ThePlayer
        local activeitem = ThePlayer and ThePlayer.replica and ThePlayer.replica.inventory and ThePlayer.replica.inventory.classified and ThePlayer.replica.inventory.classified:GetActiveItem()

        if not activeitem or not (
               activeitem:HasTag("wallbuilder")
            or activeitem:HasTag("fencebuilder")
            or activeitem:HasTag("gatebuilder")
            or activeitem:HasTag("groundtile")
            ) then
            x,_,z = GetSnap(_G.Vector3(x, 0, z)):Get()
			rotation = GetRotate(rotation)
        end
    end

    original_SendRPCToServer(rpc, action_code, x, z, target, rotation, ...)
end
]]
_G.cl_setsr = function(radius, segments)
    if _G.ThePlayer == nil then return false end

    if type(radius) == "number" then
        _radius = radius
    end

    if type(_segments) == "number" then
        _segments = segments
    end

	print("[Snapping Round] sets radius, segments", _radius, _segments)

    if _coords == nil then return false end
    RemovePoints()
    BuildRound()
end

_G.cl_unsetsr = function()
    if _centerinst == nil then return false end
    RemovePoints()
    _coords = nil
end

_G.TheInput:AddKeyDownHandler(_G[SNAP_KEY], function(key)
    if _G.ThePlayer == nil then return false end
    local inst = _G.TheInput:GetWorldEntityUnderMouse()

    RemovePoints()
    _coords = nil

    if inst ~= nil then
        local x, y, z = inst.Transform:GetWorldPosition()
        _coords = {x = x, z = z}
        BuildRound()
    end
end)