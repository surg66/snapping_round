local assets =
{
	Asset("ANIM", "anim/buildgridplacer.zip"),
}

local function fn()
    local inst = CreateEntity()

	inst:AddTag("FX")
	inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
	inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

	inst.AnimState:SetBank("buildgridplacer")
    inst.AnimState:SetBuild("buildgridplacer")
    inst.AnimState:PlayAnimation("on", true)
	
	inst.AnimState:SetLightOverride(1)
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	inst.Transform:SetScale(1.7,1.7,1.7)
	
	inst:AddTag("snappinground")

    return inst
end

return Prefab("snappinground", fn, assets)
